#!/bin/bash

# path to your Python script
python_script="/home/vtpldedpy/Udaan_indic/udaan-deploy-pipeline/ocr/cstt_ocr/main.py"

# [""]="-im  -l  -m  -s  -e "
declare -A file_args=(
    # ["/home/vtpldedpy/Udaan_indic/udaan-deploy-pipeline/pdf_upload/Common_Medical_Terms_Phrases_English-Odiya-Hindi_.pdf"]="-im Common_Medical_Terms_Phrases_English-Odiya-Hindi -l ori+eng+devanagari -m 5 -s 7 -e 200"
    # ["/home/vtpldedpy/Udaan_indic/udaan-deploy-pipeline/pdf_upload/Definitional_Journalism_2023.pdf"]="-im Definitional_Journalism_2023 -l devanagari+eng  -m 2  -s 17 -e 265"
    # ["/home/vtpldedpy/Udaan_indic/udaan-deploy-pipeline/pdf_upload/Fundamental_Admin_Term_Eng_Punjabi_2023.pdf"]="-im Fundamental_Admin_Term_Eng_Punjabi_2023 -l pan+eng -m 4 -s 10 -e 93"
    # ["/home/vtpldedpy/Udaan_indic/udaan-deploy-pipeline/pdf_upload/Definitional_dictionary_of_Jain_Philosophy_PED-956.pdf"]=" -im Definitional_dictionary_of_Jain_Philosophy_PED-956 -l eng+devanagari -m 7 -s 10 -e 232"
    ["/home/vtpldedpy/Udaan_indic/udaan-deploy-pipeline/pdf_upload/Engineering_A-E_PED-996.pdf"]=" -im Engineering_A-E_PED-996 -l eng+devanagari -m 6 -s 9 -e 174"
    ["/home/vtpldedpy/Udaan_indic/udaan-deploy-pipeline/pdf_upload/Engineering_F-Q_PED_997.pdf"]=" -im Engineering_F-Q_PED_997 -l eng+devanagari -m 6 -s 9 -e 169"
    ["/home/vtpldedpy/Udaan_indic/udaan-deploy-pipeline/pdf_upload/Glossary_of_Electronics_English-Hindi.pdf"]=" -im Glossary_of_Electronics_English-Hindi -l eng+devanagari -m 6 -s 16 -e 113"
)

for file in "${!file_args[@]}"; do
    file_arguments="${file_args[$file]}"
    command="python $python_script -i $file $file_arguments"
    echo "Running command: $command"
    $command

    echo "--------------------------------------------------------"
done
